# Tasks of the Community Team

The **Community Team** organizes the interaction with and within the community around Emergency RAVE. This includes
* [our Website](https://emergency-rave.gitlab.io/about-erave/),
* [Telegram Group](https://t.me/joinchat/HPKP8kq2iuSvD9DkFK_W5g), 
* [our LinkedIn "company" page](https://www.linkedin.com/company/e-rave/),
* [help in getting you involved!](https://emergency-rave.gitlab.io/about-erave/get-involved/)

At this point in time, the most important input you can give to us is this: If you are involved in developing solutions to mitigate the impact of COVID-19 on the most vulnerable in remote and underdeveloped parts of the planet: Tell us how we can help! We create structures and tools to make your work more efficient, but we we need to make sure that we don't run in the wrong direction!

## Membership
To join the Community Team, please sign in to GitLab, open https://gitlab.com/emergency-rave/community-team/team-tasks/ and then click on `Request Access`.

![image](https://gitlab.com/emergency-rave/guideline-team/team-tasks/-/wikis/uploads/7f29b8c4daabce5b058620959422851d/image.png)

## Resources
* [Website](https://emergency-rave.gitlab.io/about-erave/)
* [Telegram Group](https://t.me/joinchat/HPKP8kq2iuSvD9DkFK_W5g)
* [LinkedIn "company" page](https://www.linkedin.com/company/e-rave/)
* [Get involved!](https://emergency-rave.gitlab.io/about-erave/get-involved/)
